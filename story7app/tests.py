from django.test import TestCase, LiveServerTestCase
from .views import index, confirmView
from django.urls import resolve
from django.test.client import Client
from selenium import webdriver
from .models import StatusModel
from .forms import StatusForm, StatusForm2
from unittest.mock import MagicMock
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time

# Create your tests here.

class IndexTest(TestCase):

###############################################################################
    def test_root_url_resolves_to_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_root_url_resolves_to_confirmation_func(self):
        found = resolve('/confirm')
        self.assertEqual(found.func, confirmView)


    def test_index_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_confirm_url_is_exist(self):
        response = Client().get('/confirm')
        self.assertEqual(response.status_code,200)

    def test_cofirmation_template(self):
        response = Client().get('/confirm')
        self.assertTemplateUsed(response, 'confirmation.html')

    def test_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
#####################################################################################################

    def test_model_statusModel(self):
        new = StatusModel.objects.create(username = 'Rafadana', status = "test123" )
        statusModelCount = StatusModel.objects.all().count()
        self.assertTrue(isinstance(new, StatusModel))
        self.assertEqual(statusModelCount,1)
        self.assertTrue(new.__str__(), new.status)

######################################################################################################

    def test_form_Confirmation_is_valid(self):
        data = {'username' : 'rafadana', 'status' : 'mantap'}
        response = self.client.post('/confirm', data, follow = True)
        html_response = response.content.decode('utf8')
        self.assertIn('rafadana', html_response)
        self.assertIn('mantap', html_response)
    
    def test_post_confirm_then_save_(self):
        self.client = Client()
        self.client.post('/confirm', {'username' : 'rafadana', 'status' : 'mantap','save':'Save'})
        response = StatusModel.objects.all().count()
        self.assertEqual(response, 1)

    def test_post_confirm_then_notsave_(self):
        self.client = Client()
        self.client.post('/confirm', {'username' : 'rafadana', 'status' : 'mantap','notsave':'Notsave'})
        response = StatusModel.objects.all().count()
        self.assertEqual(response, 0)
       

    def test_form_Index_is_valid(self):
        form = {'username' : 'nameTest','status' : 'statusTest'}
        response = self.client.post('/', {'form' : form})
        html_response = response.content.decode()
        self.assertNotIn('nameTest', html_response)
        self.assertNotIn('statusTest', html_response)
        

    def test_display_status(self):
        post_data = Client().post('/', data = {'username' : 'nameTest','status' : 'statusTest'})
        self.assertEqual(post_data.status_code,200)

    def test_form_add_status(self):
        form = StatusForm({'username' : 'nameTest','status' : 'statusTest'})
        self.assertTrue(form.is_valid())

   


class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')


    def tearDown(self):
        self.browser.quit()

    def test_index_title(self):
        time.sleep(3)
        self.browser.get(self.live_server_url)
        self.assertIn('Hello', self.browser.title)

    def test_index_content_title(self):
        time.sleep(3)
        self.browser.get(self.live_server_url)
        self.assertIn('POST YOUR STATUS', self.browser.page_source)

    def test_insert_save_found(self):
        time.sleep(3)
        self.browser.get(self.live_server_url)
        time.sleep(3)
        name = self.browser.find_element_by_name('username')
        status = self.browser.find_element_by_name('status')
        button = self.browser.find_element_by_tag_name('button')
        
        name.send_keys('rafadana')
        status.send_keys('pepega')
        button.click()
        time.sleep(3)
        self.assertIn("Hello, world!", self.browser.title)
        save = self.browser.find_element_by_name('save')
        save.click()
        self.assertEqual(self.browser.current_url, self.live_server_url + "/")
        assert 'rafadana' in self.browser.page_source
        assert 'pepega' in self.browser.page_source

    def test_insert_save_not_found(self):
        time.sleep(3)
        self.browser.get(self.live_server_url)
        time.sleep(3)
        name = self.browser.find_element_by_name('username')
        status = self.browser.find_element_by_name('status')
        button = self.browser.find_element_by_tag_name('button')
        
        name.send_keys('rafadana')
        status.send_keys('pepega')
        button.click()
        time.sleep(3)
        self.assertIn("Hello, world!", self.browser.title)
        save = self.browser.find_element_by_name('notsave')
        save.click()
        self.assertEqual(self.browser.current_url, self.live_server_url + '/')
        self.assertNotIn("rafadana", self.browser.page_source)
        self.assertNotIn("pepega", self.browser.page_source)

    def test_change_colour(self):
        time.sleep(3)
        self.browser.get(self.live_server_url)
        time.sleep(3)
        name = self.browser.find_element_by_name('username')
        status = self.browser.find_element_by_name('status')
        button = self.browser.find_element_by_tag_name('button')
        
        name.send_keys('rafadana')
        status.send_keys('pepega')
        button.click()
        time.sleep(3)
        self.assertIn("Hello, world!", self.browser.title)
        save = self.browser.find_element_by_name('save')
        save.click()
        checkChange = self.browser.find_element_by_name('statusTitle')
        checkColour = checkChange.value_of_css_property('color')
        self.assertEqual(checkColour, 'rgba(255, 145, 147, 1)' )
        changeButton = self.browser.find_element_by_name('change')
        changeButton.click()
        checkChange = self.browser.find_element_by_name('statusTitle')
        checkColour = checkChange.value_of_css_property('color')
        self.assertEqual(checkColour, 'rgba(0, 0, 0, 1)')

        


