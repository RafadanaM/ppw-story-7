from django.urls import include, path
from .views import index, confirmView

urlpatterns = [
    path('', index, name = 'index'),
    path('confirm', confirmView, name = 'confirm'),
]