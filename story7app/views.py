from django.shortcuts import render, redirect

from .models import StatusModel
from .forms import StatusForm, StatusForm2

# Create your views here.

def index(request):
    
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            response = {'form' : form}
            return render(request, 'confirmation.html', response)

    else:

        form = StatusForm()
    statusList = StatusModel.objects.all()

    response = {'form' : form, 'statusList' : statusList}
    return render(request, 'index.html', response)

def confirmView(request):
    form = StatusForm2(request.POST)
    if request.method == 'POST':
        if form.is_valid():
            if request.POST.get('save'):
                form.save()
                return redirect('/')
            elif request.POST.get('notsave'):
                return redirect('/')


    response = {'form' : form}


    return render(request, 'confirmation.html', response)

