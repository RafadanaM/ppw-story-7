from django import forms
from .models import StatusModel

class StatusForm(forms.ModelForm):
    class Meta:
        model = StatusModel
        fields = ('username','status')
        widgets = {'username' : forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Name', 'name' : 'username'}), 
                    'status' : forms.TextInput(attrs={'class': 'form-control',  'placeholder' : 'Status..', 'name' : 'status'})} 

class StatusForm2(forms.ModelForm):
    class Meta:
        model = StatusModel
        fields = ('username','status')
        widgets = {'username' :  forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'NAME'}),
                    'status' : forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'TEST'}),
                    'username' : forms.HiddenInput(),
                    'status' : forms.HiddenInput(),} 
 

