from django.db import models

# Create your models here.

class StatusModel(models.Model):
    username = models.CharField(max_length = 255)
    status = models.CharField(max_length = 255) 

    def __str__(self):
        return self.status